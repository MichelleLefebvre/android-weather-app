package com.example.miche.simple_android_app;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Model {
    String zip;
    String city;
    String state;


    final String APIKEY = "1655f919bbcd29ed";
    JsonElement jse;
    JsonElement hourlyJSE;
    JsonElement radarJSE;
    JsonElement astroJSE;

    public Model(){}

    public Model(String z) {
        this.zip = z;

    }

    public Model(String c, String s) {
        this.city = c;
        this.state = s;

    }

    public String getTempF() {
        if (this.jse == null) {
            this.fetch();
        }
        String temp = this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f")
                .getAsString();

        return temp + "°";
    }


    public String getTempC() {
        if (this.jse == null) {
            this.fetch();
        }
        String temp = this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_c")
                .getAsString();

        return temp + "°";

    }

    public String getIcon() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon")
                .getAsString();
    }

    public String[] getNextFiveIcons() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] nextFiveIcons = new String[5];
        for (int i = 0; i < 5; i++) {
            nextFiveIcons[i] = this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("icon").getAsString();
        }
        return nextFiveIcons;
    }


    public String getCity() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();

    }

    public String getWeekday() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
    }

    public String[] getNextFiveDays() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] nextDays = new String[5];
        for (int i = 0; i < 5; i++) {
            nextDays[i] = this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
        }
        return nextDays;
    }


    public String getTime() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();

    }

    public String getWindSpeed() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_mph").getAsString();

    }

    public String getWindGust() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_gust_mph").getAsString();

    }

    public String getWindDirection() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_dir").getAsString();

    }

    public String getWindPressure() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_in").getAsString();

    }

    public String getVisibility() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();

    }

    public String getHumidity()
    {
        if (this.jse == null)
        {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("relative_humidity").getAsString();

    }

    public String getChanceOfRain() ////////////////////////
    {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();

    }

    public String getDailyHighF() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDailyHighC() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString();
    }

    public String getDailyLowF() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDailyLowC() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("low").getAsJsonObject().get("celsius").getAsString();
    }

    public String[] getFiveDayHighF() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] tempFiveDays = new String[5];
        for (int i = 0; i < 5; i++) {
            tempFiveDays[i] = this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
        }
        return tempFiveDays;
    }

    public String[] getFiveDayHighC() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] tempFiveDays = new String[5];
        for (int i = 0; i < 5; i++) {
            tempFiveDays[i] = this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString();
        }
        return tempFiveDays;
    }

    public String[] getFiveDayLowF() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] tempFiveDays = new String[5];
        for (int i = 0; i < 5; i++) {
            tempFiveDays[i] = this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
        }
        return tempFiveDays;
    }

    public String[] getFiveDayLowC() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] tempFiveDays = new String[5];
        for (int i = 0; i < 5; i++) {
            tempFiveDays[i] = this.jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("low").getAsJsonObject().get("celsius").getAsString();
        }
        return tempFiveDays;
    }

    public String[] getHourlyTempsF() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] hourlyTemps = new String[7];
        for (int i = 0; i < 7; i++) {
            hourlyTemps[i] = this.hourlyJSE.getAsJsonObject().get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
        }
        return hourlyTemps;
    }

    public String[] getHourlyTempsC() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] hourlyTemps = new String[7];
        for (int i = 0; i < 7; i++) {
            hourlyTemps[i] = this.hourlyJSE.getAsJsonObject().get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("temp").getAsJsonObject().get("metric").getAsString();
        }
        return hourlyTemps;
    }

    public String[] getHourlyTimes() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] hourlyTimes = new String[7];
        for (int i = 0; i < 7; i++) {
            hourlyTimes[i] = this.hourlyJSE.getAsJsonObject().get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("civil").getAsString();
        }
        return hourlyTimes;
    }

    public String[] getHourlyIcons() {
        if (this.jse == null) {
            this.fetch();
        }
        String[] hourlyIcons = new String[7];
        for (int i = 0; i < 7; i++) {
            hourlyIcons[i] = this.hourlyJSE.getAsJsonObject().get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("icon").getAsString();
        }
        return hourlyIcons;
    }

    public String getRadar()
    {

        if(zip == null) // City and state
        {
            city = city.replace(' ','_');
           return "http://api.wunderground.com/api/" + APIKEY + "/radar/satellite/q/" + state + "/" + city +".png";
        }

        return "http://api.wunderground.com/api/" + APIKEY + "/radar/satellite/q/" + zip +".png";

    }



    public String getSunrise()
    {
        if(this.astroJSE == null)
        {
            this.fetch();
        }
        String sunriseHour = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunrise")
                .getAsJsonObject().get("hour").getAsString();
        String sunriseMin = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunrise")
                .getAsJsonObject().get("minute").getAsString();

        String sunrise = sunriseHour + ":" + sunriseMin + " AM";

        return sunrise;
    }
    public String getSunset()
    {
        if(this.astroJSE == null)
        {
            this.fetch();
        }
        String sunsetHour = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunset")
                .getAsJsonObject().get("hour").getAsString();
        String sunsetMin = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunset")
                .getAsJsonObject().get("minute").getAsString();

        String sunset = sunsetHour + ":" + sunsetMin + " PM";

        return sunset;
    }
    public String getFeelsLikeF() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("feelslike_f").getAsString();
    }
    public String getFeelsLikeC() {
        if (this.jse == null) {
            this.fetch();
        }
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("feelslike_c").getAsString();
    }
    public String getAmountRain(){
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("precip_today_in").getAsString();
    }



    public void fetch() {
        String weatherRequestTenDay;
        String weatherRequestHourly;
        String radarRequest;
        String astroRequest;
        String formattedCity = "";

        if (zip == null) { //input is city
            formattedCity = city.replace(' ', '_');
            state = state.replace(' ', '_');
            weatherRequestTenDay = "http://api.wunderground.com/api/" + APIKEY + "/conditions/forecast10day/q/" + state + "/" + formattedCity + ".json";
            weatherRequestHourly = "http://api.wunderground.com/api/" + APIKEY + "/hourly/q/" + state + "/" + formattedCity + ".json";
            radarRequest = "http://api.wunderground.com/api/" + APIKEY + "/radar/satellite/q/" + state + "/" + formattedCity +".json";
            astroRequest = "http://api.wunderground.com/api/" + APIKEY + "/astronomy/q/"+ state + "/" + formattedCity +".json";
            //http://api.wunderground.com/api/1655f919bbcd29ed/astronomy/q/Australia/Sydney.json
            //http://api.wunderground.com/api/1655f919bbcd29ed/conditions/q/CA/Rocklin.json
            //http://api.wunderground.com/api/1655f919bbcd29ed/radar/q/CA/New_York.json
        } else { //input is zipcode
            weatherRequestTenDay = "http://api.wunderground.com/api/" + APIKEY + "/conditions/forecast10day/q/" + zip + ".json";
            weatherRequestHourly = "http://api.wunderground.com/api/" + APIKEY + "/hourly/q/" + zip + ".json";
            radarRequest = "http://api.wunderground.com/api/" + APIKEY + "/radar/satellite/q/" + zip +".json";
            astroRequest = "http://api.wunderground.com/api/" + APIKEY + "/astronomy/q/"+ zip +".json";

        }
        try {
            URL weatherURL = new URL(weatherRequestTenDay);
            InputStream is = weatherURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            this.jse = new JsonParser().parse(br);

            URL hourlyWeatherURL = new URL(weatherRequestHourly);
            InputStream is2 = hourlyWeatherURL.openStream();
            InputStreamReader isr2 = new InputStreamReader(is2);
            BufferedReader br2 = new BufferedReader(isr2);
            this.hourlyJSE = new JsonParser().parse(br2);

            URL radarURL = new URL(radarRequest);
            InputStream is3 = radarURL.openStream();
            InputStreamReader isr3 = new InputStreamReader(is3);
            BufferedReader br3 = new BufferedReader(isr3);
            this.radarJSE = new JsonParser().parse(br3);

            URL astroURL = new URL(astroRequest);
            InputStream is4 = astroURL.openStream();
            InputStreamReader isr4 = new InputStreamReader(is4);
            BufferedReader br4 = new BufferedReader(isr4);
            this.astroJSE = new JsonParser().parse(br4);

//            URL plannerURL = new URL(plannerRequest);
//            InputStream is4 = astroURL.openStream();
//            InputStreamReader isr4 = new InputStreamReader(is4);
//            BufferedReader br4 = new BufferedReader(isr4);
//            this.astroJSE = new JsonParser().parse(br4);

        } catch (MalformedURLException e) {
            System.out.println("URL is not formed well");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Got IO exception");
            e.printStackTrace();
        }


    }

    public static void main(String[] args)
    {
        Model m = new Model("95678");
        Model s = new Model("San Diego", "CA");

        //Highlight over which tests you want to run and press ctrl + / to uncomment and run.

        System.out.println("Get city: " + m.getCity());
        System.out.println("Get city: " + s.getCity());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get temp F: " + m.getTempF());
        System.out.println("Get temp F: " + s.getTempF());

        System.out.println("------------------------------"); // Formatting


        System.out.println("Get temp C: " + m.getTempC());
        System.out.println("Get temp C: " + s.getTempC());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get icon: " + m.getIcon());
        System.out.println("Get icon: " + s.getIcon());

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 5; i++){
            System.out.println("Get next five icons: " + m.getNextFiveIcons()[i]);
        }
        for (int i = 0; i < 5; i++){
            System.out.println("Get next five icons: " + s.getNextFiveIcons()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get weekday: " + m.getWeekday());
        System.out.println("Get weekday: " + s.getWeekday());

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 5; i++){
            System.out.println("Get next five days: " + m.getNextFiveDays()[i]);
        }
        for (int i = 0; i < 5; i++){
            System.out.println("Get next five days: " + s.getNextFiveDays()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get time updated: " + m.getTime());
        System.out.println("Get time updated: " + s.getTime());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get wind speed: " + m.getWindSpeed());
        System.out.println("Get wind speed: " + s.getWindSpeed());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get wind gust: " + m.getWindGust());
        System.out.println("Get wind gust: " + s.getWindGust());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get wind direction: " + s.getWindDirection());
        System.out.println("Get wind direction: " + m.getWindDirection());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get wind pressure: " + m.getWindPressure());
        System.out.println("Get wind pressure: " + s.getWindPressure());

        System.out.println("------------------------------"); // Formatting



        System.out.println("Get visibility: " + m.getVisibility());
        System.out.println("Get visibility: " + s.getVisibility());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get chance of rain: " + m.getChanceOfRain());
        System.out.println("Get chance of rain: " + s.getChanceOfRain());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get daily high F: " + m.getDailyHighF());
        System.out.println("Get daily high F: " + s.getDailyHighF());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get daily high C: " + m.getDailyHighC());
        System.out.println("Get daily high C: " + s.getDailyHighC());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get daily low F: " + m.getDailyLowF());
        System.out.println("Get daily low F: " + s.getDailyLowF());

        System.out.println("------------------------------"); // Formatting

        System.out.println("Get daily low C: " + m.getDailyLowC());
        System.out.println("Get daily low C: " + s.getDailyLowC());

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 5; i++){
            System.out.println("Get five day high F: " + m.getFiveDayHighF()[i]);
        }
        for (int i = 0; i < 5; i++){
            System.out.println("Get five day high F: " + s.getFiveDayHighF()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 5; i++){
            System.out.println("Get five day high C: " + m.getFiveDayHighC()[i]);
        }
        for (int i = 0; i < 5; i++){
            System.out.println("Get five day high C: " + s.getFiveDayHighC()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 5; i++){
            System.out.println("Get five day low F: " + m.getFiveDayLowF()[i]);
        }
        for (int i = 0; i < 5; i++){
            System.out.println("Get five day low F: " + s.getFiveDayLowF()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 5; i++){
            System.out.println("Get five day low C: " + m.getFiveDayLowC()[i]);
        }
        for (int i = 0; i < 5; i++){
            System.out.println("Get five day low C: " + s.getFiveDayLowC()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 7; i++)
        {
            System.out.println("Next 7 hourly temps: " + m.getHourlyTempsF()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 7; i++)
        {
            System.out.println("Next 7 hourly times: " + m.getHourlyTimes()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        for (int i = 0; i < 7; i++)
        {
            System.out.println("Next 7 hourly icons: " + m.getHourlyIcons()[i]);
        }

        System.out.println("------------------------------"); // Formatting

        System.out.println(s.getRadar());
        System.out.println(m.getRadar());


        System.out.println("------------------------------"); // Formatting

        System.out.println(m.getSunrise());
        System.out.println(s.getSunrise());

        System.out.println("------------------------------"); // Formatting
        System.out.println(m.getSunset());
        System.out.println(s.getSunset());


    }

}
