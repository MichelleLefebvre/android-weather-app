package com.example.miche.simple_android_app;

import android.os.Bundle;
import android.os.StrictMode;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String city;
    String conditionsRequest;
    String forcastRequest;
    String radarRequest;
    String astroRequest;


    final String APIKEY = "1655f919bbcd29ed";

    JsonElement jse;
    JsonElement forcastJSE;
    JsonElement radarJSE;
    JsonElement astroJSE;
    public EditText cityZip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cityZip = (EditText) findViewById(R.id.cityZip);
        cityZip.setOnClickListener(this);




        // "Solve" network on main thread exception
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }
    @Override
    public void onClick(View v)
    {
        cityZip.setText("");

    }

    public void fetch(View view){
        EditText cityZip = findViewById(R.id.cityZip); //Reference text
        TextView tempF = findViewById(R.id.tempF); //Reference temp text field
        String cityZipText = cityZip.getText().toString();

        // Create autocomplete request and return city
        try {
            JsonElement autoCompleteElement;
            String autoCompleteRequest = "http://autocomplete.wunderground.com/aq?query=" + cityZipText + "&c=US";

            URL weatherURL = new URL(autoCompleteRequest);
            InputStream is = weatherURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            autoCompleteElement = new JsonParser().parse(br);

            try {
                city = autoCompleteElement.getAsJsonObject().get("RESULTS").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString();
            }
            catch (IndexOutOfBoundsException e) {
                // When nothing gets returned from request i.e. someone searches jfdglsdahgli
                city = "City not found";
                cityZip.setText(city);
                return;
            }
        }
        catch (MalformedURLException e) {
            System.out.println("URL is not formed well");
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("Got IO exception");
            e.printStackTrace();
        }

        //Example Urls:
        //http://api.wunderground.com/api/1655f919bbcd29ed/astronomy/q/Australia/Sydney.json
        //http://api.wunderground.com/api/1655f919bbcd29ed/conditions/q/CA/Rocklin.json
        //http://api.wunderground.com/api/1655f919bbcd29ed/radar/q/CA/New_York.json

        //Create request urls
        conditionsRequest = "http://api.wunderground.com/api/" + APIKEY + "/conditions/q/" + city + ".json";
        forcastRequest = "http://api.wunderground.com/api/" + APIKEY + "/conditions/forecast10day/q/" + city + ".json";
//      weatherRequestHourly = "http://api.wunderground.com/api/" + APIKEY + "/hourly/q/" + city + ".json";
        radarRequest = "http://api.wunderground.com/api/" + APIKEY + "/satellite/q/" + city + ".gif";
        astroRequest = "http://api.wunderground.com/api/" + APIKEY + "/astronomy/q/" + city + ".json";
//
        try {
            URL weatherURL = new URL(conditionsRequest);
            InputStream is = weatherURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            this.jse = new JsonParser().parse(br);

            URL weatherURL2 = new URL(forcastRequest);
            InputStream is2 = weatherURL2.openStream();
            InputStreamReader isr2 = new InputStreamReader(is2);
            BufferedReader br2 = new BufferedReader(isr2);
            this.forcastJSE = new JsonParser().parse(br2);
//
//            URL radarURL = new URL(radarRequest);
//            InputStream is3 = radarURL.openStream();
//            InputStreamReader isr3 = new InputStreamReader(is3);
//            BufferedReader br3 = new BufferedReader(isr3);
//            this.radarJSE = new JsonParser().parse(br3);
//
            URL astroURL = new URL(astroRequest);
            InputStream is4 = astroURL.openStream();
            InputStreamReader isr4 = new InputStreamReader(is4);
            BufferedReader br4 = new BufferedReader(isr4);
            this.astroJSE = new JsonParser().parse(br4);

        } catch (MalformedURLException e) {
            System.out.println("URL is not formed well");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Got IO exception");
            e.printStackTrace();
        }

        //Set gui
        cityZip.setText(city);
        tempF.setText(this.getTempF());

//        //Set Icons
//        for(int i = 1; i <= 5; i++){
//            ImageView icon = findViewById(R.id.icon + i);
//            icon.setImageIcon(R.id.);
//        }

        //Set high
        TextView high1 = findViewById(R.id.high1);
        TextView high2 = findViewById(R.id.high2);
        TextView high3 = findViewById(R.id.high3);
        TextView high4 = findViewById(R.id.high4);
        TextView high5 = findViewById(R.id.high5);

        high1.setText("H: " + this.getFiveDayHighF()[0]);
        high2.setText("H: " + this.getFiveDayHighF()[1]);
        high3.setText("H: " + this.getFiveDayHighF()[2]);
        high4.setText("H: " + this.getFiveDayHighF()[3]);
        high5.setText("H: " + this.getFiveDayHighF()[4]);

        //Set low
        TextView low1 = findViewById(R.id.low1);
        TextView low2 = findViewById(R.id.low2);
        TextView low3 = findViewById(R.id.low3);
        TextView low4 = findViewById(R.id.low4);
        TextView low5 = findViewById(R.id.low5);

        low1.setText("L: " + this.getFiveDayLowF()[0]);
        low2.setText("L: " + this.getFiveDayLowF()[1]);
        low3.setText("L: " + this.getFiveDayLowF()[2]);
        low4.setText("L: " + this.getFiveDayLowF()[3]);
        low5.setText("L: " + this.getFiveDayLowF()[4]);

        //Set days
        TextView day1 = findViewById(R.id.day1);
        TextView day2 = findViewById(R.id.day2);
        TextView day3 = findViewById(R.id.day3);
        TextView day4 = findViewById(R.id.day4);
        TextView day5 = findViewById(R.id.day5);

        day1.setText(this.getNextFiveDays()[0]);
        day2.setText(this.getNextFiveDays()[1]);
        day3.setText(this.getNextFiveDays()[2]);
        day4.setText(this.getNextFiveDays()[3]);
        day5.setText(this.getNextFiveDays()[4]);

        TextView feelslike = findViewById(R.id.feelsLike);
        TextView wind =  findViewById(R.id.Wind);
        TextView rainAmount = findViewById(R.id.RainAmount);
        TextView humidity = findViewById(R.id.Humidity);
        TextView high = findViewById(R.id.High);
        TextView low = findViewById(R.id.Low);
        TextView sunrise = findViewById(R.id.Sunrise);
        TextView visibility = findViewById(R.id.Visibility);
        TextView pressure = findViewById(R.id.Pressure);
        TextView sunset = findViewById(R.id.Sunset);

        feelslike.setText("Feels Like: " + this.getFeelsLikeF() + "°");
        wind.setText("Wind Speed: " + this.getWindSpeed() + " mph");
        rainAmount.setText("Rain Amount: " + this.getAmountRain() + " in");
        humidity.setText("Humidity: "+this.getHumidity() );
        high.setText("Daily High: " + this.getDailyHighF() + "°");
        low.setText("Daily Low: " + this.getDailyLowF() + "°");
        sunrise.setText("Sunrise: " + this.getSunrise());
        visibility.setText("Visibility: " + this.getVisibility() + " mi");
        pressure.setText("Pressure: " + this.getWindPressure() + " inHg");
        sunset.setText("Sunset: " +this.getSunset());



        String[] iconResources = this.getNextFiveIcons();
        int[] iconIDs = new int[5];
        for(int i = 0; i < 5; i++){
            iconIDs[i] = getResources().getIdentifier(iconResources[i], "drawable", getPackageName());
        }

        ImageView icon1 = findViewById(R.id.icon1);
        ImageView icon2 = findViewById(R.id.icon2);
        ImageView icon3 = findViewById(R.id.icon3);
        ImageView icon4 = findViewById(R.id.icon4);
        ImageView icon5 = findViewById(R.id.icon5);

        icon1.setImageResource(iconIDs[0]);
        icon2.setImageResource(iconIDs[1]);
        icon3.setImageResource(iconIDs[2]);
        icon4.setImageResource(iconIDs[3]);
        icon5.setImageResource(iconIDs[4]);






        this.updateSatImage();
    }

    //    public void getSatImageURL(){ return radarRequest };

    public String getTempF() {
        String temp = this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f")
                .getAsString();

        return temp + "°";
    }

    public String[] getNextFiveIcons() {
        String[] nextFiveIcons = new String[5];
        for (int i = 0; i < 5; i++) {
            nextFiveIcons[i] = this.forcastJSE.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("icon").getAsString();
        }
        return nextFiveIcons;
    }

    public String[] getFiveDayHighF() {
        String[] tempFiveDays = new String[5];
        for (int i = 0; i < 5; i++) {
            tempFiveDays[i] = this.forcastJSE.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
        }
        return tempFiveDays;
    }

    public String[] getFiveDayLowF() {
        String[] tempFiveDays = new String[5];
        for (int i = 0; i < 5; i++) {
            tempFiveDays[i] = this.forcastJSE.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
        }
        return tempFiveDays;
    }

    public String[] getNextFiveDays() {
        String[] nextDays = new String[5];
        for (int i = 0; i < 5; i++) {
            nextDays[i] = this.forcastJSE.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
        }
        return nextDays;
    }

    public String getFeelsLikeF()
    {

        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("feelslike_f").getAsString();
    }


    public String getVisibility() {

        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();

    }

    public String getHumidity()
    {

        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("relative_humidity").getAsString();

    }

    public String getChanceOfRain()
    {

        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();

    }
    public String getWindSpeed() {

        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_mph").getAsString();

    }
    public String getDailyHighF() {

        return this.forcastJSE.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }
    public String getDailyLowF() {

        return this.forcastJSE.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }
    public String getAmountRain(){
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("precip_today_in").getAsString();
    }
    public String getSunset()
    {

        String sunsetHour = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunset")
                .getAsJsonObject().get("hour").getAsString();
        String sunsetMin = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunset")
                .getAsJsonObject().get("minute").getAsString();

        String sunset = sunsetHour + ":" + sunsetMin + " PM";

        return sunset;
    }

    public String getSunrise()
    {

        String sunriseHour = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunrise")
                .getAsJsonObject().get("hour").getAsString();
        String sunriseMin = this.astroJSE.getAsJsonObject().get("sun_phase").getAsJsonObject().get("sunrise")
                .getAsJsonObject().get("minute").getAsString();

        String sunrise = sunriseHour + ":" + sunriseMin + " AM";

        return sunrise;
    }

    public String getWindPressure() {
        return this.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_in").getAsString();

    }


    public void updateSatImage(){
        WebView satelliteView = findViewById(R.id.satelliteView);
        satelliteView.loadUrl(radarRequest);
    }
}
